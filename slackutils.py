import os
from slackclient import SlackClient

BOT_NAME = 'snekbot'

slack_client = SlackClient(os.environ.get('SLACK_BOT_TOKEN'))


def get_user_id(username):
	api_call = slack_client.api_call("users.list")
	if api_call.get('ok'):
		users = api_call.get('members')
		for user in users:
			if 'name' in user and user.get('name') == username:
				return user['id']
		return None
	else:
		print("Auth failure")
		return None


def get_channel_id(channel):
	api_call = slack_client.api_call("channels.list")
	if api_call.get('ok'):
		channels = api_call.get('channels')
		for c in channels:
			if 'name' in c and c.get('name') == channel:
				return c['id']
		return None
	else:
		print("Auth failure")
		return None


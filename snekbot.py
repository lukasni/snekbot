#! /usr/bin/env python
import os
import time
from slackclient import SlackClient
import slackutils

import wolfram
import markov_tweets
import markov_slack
import markov_dijkstra

BOT_ID = os.environ.get("SLACK_BOT_ID")
AT_BOT = "<@{}>".format(BOT_ID)

slack_client = SlackClient(os.environ.get("SLACK_BOT_TOKEN"))
print(os.environ.get("SLACK_BOT_TOKEN"))
print(os.environ.get("SLACK_BOT_ID"))


def handle_command(command, channel, user):
	options = {}
	response = "Command not recognized"
	if command.startswith('get sessiontime'):
		session_time = slack_client.api_call("channels.info", \
											 channel='session-scheduling')['channel']['topic']['value'].split('*')[1].strip()
		response = "Next session: {}".format(session_time)
	if command.startswith('set sessiontime'):
		session_time = command.split('set sessiontime')[1].strip()
		response = "Setting session time to {}".format(session_time)	
	if command.startswith('boop'):
		response = "This should be showing a random image from /r/sneks, but @niederbergerlukas is lazy" 		
	if command.startswith('query:'):
		query = command.split('query:')[1].strip()
		response = wolfram.run_query(query)
	if command.startswith('tweet in chief'):
		response = markov_tweets.make_tweet('realDonaldTrump')
		options['unfurl_media'] = False
	if command.startswith('black science man'):
		response = markov_tweets.make_tweet('neiltyson')
		options['unfurl_media'] = False
	if command.startswith('ewd'):
		response = markov_dijkstra.message()
	if command.startswith('simulate') or command.startswith('learn'):
		tokens = command.split(' ')
		name = tokens[1].strip()
		command = tokens[0].strip()
		user_id = 0
		if name == 'me':
			user_id = user
		elif name == 'snekbot':
			response = "I already know all I need to know about myself."
			user_id = user
		else:
			user_id = slackutils.get_user_id(name)
		if not user_id:
			response = "Username {} not recognized".format(name)
		if response == "Command not recognized":
			if command.startswith('simulate'):
				response = markov_slack.message(user_id)
			else:
				new_lines = markov_slack.build_corpus(slackutils.get_channel_id('general-discussion'), user_id)
				response = "Corpus built for <@{user}>. {new_lines}".format(user=user_id, new_lines=new_lines)

	slack_client.api_call("chat.postMessage", channel=channel, text=response, as_user=True, **options)


def parse_slack_output(slack_rtm_output):
	output_list = slack_rtm_output
	if output_list and len(output_list) > 0:
		for output in output_list:
			if output and 'text' in output and AT_BOT in output['text']:
				return output['text'].split(AT_BOT)[1].strip(), output['channel'], output['user']
	return None, None, None


if __name__ == "__main__":
	READ_WEBSOCKET_DELAY = 1
	if slack_client.rtm_connect():
		print("Snekbot connected and running!")
		while True:
			command, channel, user = parse_slack_output(slack_client.rtm_read())
			if command and channel:
				handle_command(command, channel, user)
			time.sleep(READ_WEBSOCKET_DELAY)
	else:
		print("Connection failed. Verify Token and Bot ID")
		print(slack_client)

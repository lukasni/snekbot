import requests
import os

APP_ID = os.environ.get("WOLFRAM_APP_ID")
API_URL = "http://api.wolframalpha.com/v1/result"


def run_query(query):
	payload = {
		'i': query,
		'units': 'metric',
		'appid': APP_ID
	}
	r = requests.get(API_URL, params=payload)

	return r.text





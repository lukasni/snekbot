from slackclient import SlackClient
import os
import markovify

import slackutils

slack_client = SlackClient(os.environ.get("SLACK_BOT_TOKEN"))

corpus_path = os.path.join("corpora","slack")
corpus_suffix="_corpus.txt"

def get_messages(channel_id, user_id, latest_message = 0):

	user_messages = []
	last_timestamp = 0
	new_latest_message = 0

	while True:
		print("Starting api call")
		api_call = slack_client.api_call('channels.history', channel=channel_id, latest=last_timestamp, oldest = latest_message, count=500)
		print("API Call complete")
		if api_call.get('ok'):
			print('Parsing page. Latest {}'.format(last_timestamp or 'None'))
			messages = api_call.get('messages')
			if messages:
				if new_latest_message == 0:
					new_latest_message = messages[0].get('ts')
				user_messages.extend([m.get('text') for m in messages if m.get('user') == user_id])
	
				last_timestamp = messages[-1].get('ts')
			if not api_call.get('has_more'):
				break
		else:
			print("api call failed")
			print(api_call)
			break
	
	return (user_messages, new_latest_message)

def build_corpus(channel_id, user_id):
	corpus_file = os.path.join(corpus_path,user_id+corpus_suffix)
	latest_message = 0
	old_messages = []

	if os.path.isfile(corpus_file):
		with open(corpus_file, 'r') as f:
			old_messages = f.readlines()
		latest_message = old_messages.pop(0)
	
	new_messages, last_timestamp = get_messages(channel_id, user_id, latest_message)

	all_messages = []
	all_messages.append(str(last_timestamp)+os.linesep)
	all_messages.extend(new_messages)
	all_messages.extend(old_messages)

	with open(corpus_file, 'w') as f:
		f.truncate()
		f.write("\n".join(all_messages))

	return "{} new messages written to corpus.".format(len(new_messages))
		
	

def message(user_id):
	corpus_file = os.path.join(corpus_path,user_id+corpus_suffix)

	if os.path.isfile(corpus_file):
	    with open(corpus_file) as f:
	        corpus = f.read()
	
	    model = markovify.NewlineText(corpus)
	
	    tweet = ">{tweet}\n>    <@{user}>".format(
	        tweet = model.make_sentence(),
	        user = user_id
	    )
	else:
		tweet = "No corpus found for that user"

	return tweet
			

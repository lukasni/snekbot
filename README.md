Snekbot
=======

Slack bot experiments
---------------------

This is a programming practice project more than anything. It consists of a very basic slack bot that calls various other web apis to generate tweets, answer questions via Wolfram|Alpha and other things.

This is _not_ a terribly good example of good code. I'm sloppy about adding error handling or even basic sanity checks. This is very much experimental. I will come back to it at some point to fix the most glaring holes but for now be warned.

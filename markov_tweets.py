import tweepy
from tweepy import OAuthHandler

import os
import markovify

consumer_key = os.environ.get("TWITTER_CONSUMER_KEY")
consumer_secret = os.environ.get("TWITTER_CONSUMER_SECRET")
access_token = os.environ.get("TWITTER_ACCESS_TOKEN")
access_secret = os.environ.get("TWITTER_ACCESS_SECRET")

corpus_path = os.path.join("corpora","twitter")
corpus_suffix = "_corpus.txt"

auth = OAuthHandler(consumer_key, consumer_secret)
auth.set_access_token(access_token, access_secret)

api = tweepy.API(auth)

def build_corpus_file(user):
	tweets = []
	new_tweets = api.user_timeline(screen_name=user, include_rts=False, count=200)
	tweets.extend(new_tweets)
	oldest = tweets[-1].id -1

	while len(new_tweets) > 0:
		new_tweets = api.user_timeline(screen_name=user, include_rts=False, count=200, max_id=oldest)
		tweets.extend(new_tweets)
		oldest = tweets[-1].id - 1

	corpus = open(os.path.join(corpus_path,user+corpus_suffix), 'w')
	corpus.truncate()

	for status in tweets:
		corpus.write(status.text + os.linesep)
	
	corpus.close()

#def build_corpus(user):
#	tweets = api.user_timeline(screen_name=user, include_rts=False, count=210)
#	corpus = ""
#
#	for status in tweets:
#		corpus += status.text + "\n"
#
#	return corpus

def make_tweet(user):
	corpus_file = os.path.join(corpus_path,user+corpus_suffix)
	if os.path.isfile(corpus_file):
		with open(os.path.join(corpus_path,user+corpus_suffix)) as f:
			corpus = f.read()
#		corpus = build_corpus(user)

		model = markovify.NewlineText(corpus)

		tweet = ">{tweet}\n>    _@{user}_".format(
			tweet = model.make_short_sentence(140),
			user = user
		)
	else:
		tweet = "No corpus found for that user"

	return tweet

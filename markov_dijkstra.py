from slackclient import SlackClient
import os
import markovify

import slackutils

slack_client = SlackClient(os.environ.get("SLACK_BOT_TOKEN"))

corpus_path = os.path.join("corpora")
corpus_suffix="dijkstra.txt"

def message():
	corpus_file = os.path.join(corpus_path,corpus_suffix)

	if os.path.isfile(corpus_file):
	    with open(corpus_file) as f:
	        corpus = f.read()
	
	    model = markovify.Text(corpus)
	
	    tweet = ">{tweet}\n>    prof. dr. Edsger W. Dijkstra".format(
	        tweet = model.make_sentence()
	    )
	else:
		tweet = "No corpus found for that user"

	return tweet
			
